from flask import Flask, render_template, request, redirect, url_for, jsonify
import random

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON')
def bookJSON():
    return jsonify(books)

@app.route('/')
def index():
    return render_template('showBook.html', dict1=books)

@app.route('/book/')
def showBook():
    return render_template('showBook.html', dict1=books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    id_arr = []
    new = {}
    if request.method == "POST":
        # Getting our book name to add and placing all current Ids in an array for sorting
        new_book = request.form['bookName']
        for i in range(len(books)):
            id_arr.append(int(books[i]['id']))

        # Sorting our ID array to find out the next available number
        id_arr.sort()

        for i in range(0, len(id_arr)):
            # If there is a gap between any two elements larger than 1, use the ith element + 1 to get the "next" available number
            try:
                if (id_arr[i] + 1) != id_arr[i+1]:
                    id_arr.append((id_arr[i] + 1))
                    new['title'] = new_book
                    new['id'] = str(id_arr[i] + 1)
                    books.append(new)
                    break
            except IndexError:
                id_arr.append(len(id_arr) + 1)
                new['title'] = new_book
                new['id'] = str(id_arr[-1])
                books.append(new)
                
        return redirect(url_for('showBook'))
    else:
        return render_template('newBook.html', books=books)

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == "POST":
        search_key = request.form['bookName']

        for dict_item in books:
            if str(book_id) in dict_item.values():
                dict_item['title'] = search_key

        return redirect(url_for('showBook'))
    else:
        return render_template('editBook.html', id=book_id, books=books)
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    remove = 0
    if request.method == "POST":
        for i in range(len(books)):
            if str(book_id) in books[i].values():
                remove = i
        books.pop(remove)
        return redirect(url_for('showBook'))
    else:   
        return render_template('deleteBook.html', id=book_id, books=books)

if __name__ == '__main__':
	app.debug = True
	app.run()
